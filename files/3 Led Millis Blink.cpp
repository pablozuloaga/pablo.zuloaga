#include <Arduino.h>

int ledPin0 =  D8;      // the number of the LED pin
int ledState0 = LOW;             // ledState used to set the LED
unsigned long previousMillis0 = 0;        // will store last time LED was updated
long OnTime0 = 750;           // milliseconds of on-time
long OffTime0 = 250;          // milliseconds of off-time

int ledPin1 =  D6;      // the number of the LED pin
int ledState1 = LOW;             // ledState used to set the LED
unsigned long previousMillis1 = 0;        // will store last time LED was updated
long OnTime1 = 250;           // milliseconds of on-time
long OffTime1 = 750;          // milliseconds of off-time
 
int ledPin2 =  D5;      // the number of the LED pin
int ledState2 = LOW;             // ledState used to set the LED
unsigned long previousMillis2 = 0;        // will store last time LED was updated
long OnTime2 = 500;           // milliseconds of on-time
long OffTime2 = 500;          // milliseconds of off-time
 
void setup() 
{
  // set the digital pin as output:
  pinMode(ledPin0, OUTPUT); 
  pinMode(ledPin1, OUTPUT);      
  pinMode(ledPin2, OUTPUT);     
  
}
 
void loop()
{
  // check to see if it's time to change the state of the LED
  unsigned long currentMillis = millis();

  if((ledState0 == HIGH) && (currentMillis - previousMillis0 >= OnTime0))
  {
    ledState0 = LOW;  // Turn it off
    previousMillis0 = currentMillis;  // Remember the time
    digitalWrite(ledPin0, ledState0);  // Update the actual LED
  }
  else if ((ledState0 == LOW) && (currentMillis - previousMillis0 >= OffTime0))
  {
    ledState0 = HIGH;  // turn it on
    previousMillis0 = currentMillis;   // Remember the time
    digitalWrite(ledPin0, ledState0);    // Update the actual LED
  }
  if((ledState1 == HIGH) && (currentMillis - previousMillis1 >= OnTime1))
  {
    ledState1 = LOW;  // Turn it off
    previousMillis1 = currentMillis;  // Remember the time
    digitalWrite(ledPin1, ledState1);  // Update the actual LED
  }
  else if ((ledState1 == LOW) && (currentMillis - previousMillis1 >= OffTime1))
  {
    ledState1 = HIGH;  // turn it on
    previousMillis1 = currentMillis;   // Remember the time
    digitalWrite(ledPin1, ledState1);    // Update the actual LED
  }
  
  if((ledState2 == HIGH) && (currentMillis - previousMillis2 >= OnTime2))
  {
    ledState2 = LOW;  // Turn it off
    previousMillis2 = currentMillis;  // Remember the time
    digitalWrite(ledPin2, ledState2);  // Update the actual LED
  }
  else if ((ledState2 == LOW) && (currentMillis - previousMillis2 >= OffTime2))
  {
    ledState2 = HIGH;  // turn it on
    previousMillis2 = currentMillis;   // Remember the time
    digitalWrite(ledPin2, ledState2);   // Update the actual LED
  }
}
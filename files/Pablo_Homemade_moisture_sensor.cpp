#include <Arduino.h>

int moistPin = 0;
int moistVal = 0;
int tooDry = 8.7*30;
int tooWet = 8.7*70;
void setup()
{
  Serial.begin(9600);
}
void loop()
{
  moistVal = analogRead(moistPin);
  int percent = moistVal/8.7;
  Serial.print(percent);
  Serial.println("% Moisture ");
  if (moistVal <= tooDry) {
    digitalWrite(4, HIGH); //Red LED
    digitalWrite(3, LOW);
    digitalWrite(2, LOW);
    Serial.println("Too Dry");
  }
  else if (moistVal >= tooWet) {
    digitalWrite(4, LOW);
    digitalWrite(3, HIGH); //Blue LED
    digitalWrite(2, LOW);
    Serial.println("Too Wet");
  }
  else {
    digitalWrite(4, LOW);
    digitalWrite(3, LOW);
    digitalWrite(2, HIGH); //Green LED
    Serial.println("Good Moisture");
  }
  delay(250);
}

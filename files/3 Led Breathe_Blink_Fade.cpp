
// JLed multi LED demo. control multiple LEDs in-sync.
// Copyright 2017 by Jan Delgado. All rights reserved.
// https://github.com/jandelgado/jled
#include <jled.h>

JLed leds[] = {
    JLed(D8).Blink(750, 250).Forever(),
    JLed(D6).Breathe(2000).Forever(),
    JLed(D5).FadeOn(1000).Forever(),
    JLed(LED_BUILTIN).Blink(500, 500).Forever()
};

JLedSequence sequence(JLedSequence::eMode::PARALLEL, leds);

void setup() { }

void loop() {
    sequence.Update();
    delay(1);
}
int trigger_pin = 2;
int echo_pin = 3;
long distance, pulse_duration;

void setup() {

Serial.begin (9600);

pinMode(trigger_pin, OUTPUT);
pinMode(echo_pin, INPUT);

digitalWrite(trigger_pin, LOW);
}

void loop() {

digitalWrite(trigger_pin, HIGH);
delayMicroseconds(10);
digitalWrite(trigger_pin, LOW);

pulse_duration = pulseIn(echo_pin, HIGH);

distance = round(pulse_duration * 0.0171);

//distance = round(pulse_duration/0.00675);

Serial.print(distance);
Serial.print("cm");
Serial.println();

delay(500);
}

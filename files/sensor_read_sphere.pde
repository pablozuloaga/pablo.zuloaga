import processing.serial.*;
String lecture = "0";
int val = 0;

Serial myPort;

void setup() { 
  size(800, 600, P3D); 
  myPort = new Serial(this, "/dev/cu.SLAB_USBtoUART", 9600);
}

void draw() { 
  if (myPort.available() > 0) { 
    lecture = myPort.readStringUntil('\n'); 
    println(lecture); 
    if (lecture != null) { 
      lecture = trim(lecture); 
      val = Integer.parseInt(lecture);
    }
  } 
  background(0);
  noStroke();
  map(float(val), 0.0, 1023.0, 0.0, 255.0); 
  //camera(mouseX, height/2, (height/2) / tan(PI/6), mouseX, height/2, 0, 0, 1, 0);
  directionalLight(val,0,val,1,0,-val/255);
  translate(width/2, height/2);

  fill(255,204,0);
  sphere(val/2);
  
  //rect(100, 350, val/2, 200); 
  //fill(255); 
  //textSize(40); 
  //text(val/10, 100+(val/2), 470);
  println(val);
}

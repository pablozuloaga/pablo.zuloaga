int cols, rows;
int scl = 20;
int h = 3000;
int w = 5000;

float flying = 0;

float[][] terrain;
  
void setup() {
  size(1280, 800, P3D);

  cols = w / scl;
  rows = h / scl;
  terrain = new float[cols][rows];

}

void draw() {
    
  flying -= 0.1;
  
    float yoff = flying;
    for (int y = 0; y < rows; y++) {
      float xoff = 0;
      for (int x = 0; x < cols; x++) {
          terrain[x][y] = map(noise(xoff,yoff), 0, 1, -200, mouseY);
          xoff += 0.1;
       }
       yoff += 0.1;
    }
  
  background(0);
  stroke(255,(map(mouseX, 0, 1280, 0, 255)),(map(mouseY, 0, 800, 0, 255)));
  fill(map(mouseY, 0, 800, 0, 255));  
  
  translate(mouseX, height/2);
  rotateX(PI/3);
  translate(-w/2, -h/2);
  for (int y = 0; y < rows-1; y++) {
    beginShape(TRIANGLE_STRIP);
    for (int x = 0; x < cols; x++) {
      vertex(x*scl, y*scl, terrain[x][y]);
      vertex(x*scl, (y+1)*scl, terrain[x][y+1]);

      //rect(x*scl, y*scl, scl, scl);
    }
    endShape();
  }
}

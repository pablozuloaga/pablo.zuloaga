---

Title: How to combat social distance

Author: Pablo Zuloaga Betancourt

Category: Health

Layout: Post

---

# Simulation as a way to combat Social Distance

#### Author: Pablo Zuloaga Betancourt

#### Introduction

As people's mobility and interaction with others are limited by the lockdown, many are looking for new ways to interact, entertain themselves and spend their free time doing something as a way to maintain their mental health stable.

Screens and balconies are now the closest contacts we can have with someone, necesity has taken us to find new ways to simulate our outdoor life as it used to happen. A lot of people are living right now in a new way of loneliness, apart from everyone and everything, confined into their homes, sometimes with not much to do. We try to have the family lunch or go out and party in a  different way, or interacting with people that we don´t really know through the balconies for the first time, even if they have always been our neighbors.

Simulation is the new normal, it is a way to trying to live a reality we miss in a new reality we have to live. We simulate spaces, scenarios, and a lot of different things we used to do before to feel safe, to feel comfortable, to be calm, and to think once in a while that nothing is going on and everything is going to be alright.

---

## Idea 1: Balcony Society

![Balcony Concert](../_pablo-zuloaga/assets/balcony_concert.png)

Balconies have become our new means of contact with the outside world, our closest path to freedom. People take advantage of them for sunbathing, reading a book, smoking a cigarette, or even changing spaces for a while and doing some of their daily chores elsewhere. 

![Balcony Philarmonic](../_pablo-zuloaga/assets/Quater_Philarmonic.mp4)

![Balcony Guitar](../_pablo-zuloaga/assets/balcony_guitar.mp4)

Artists come to the balconies to play some music for others, they take out their instruments and give the best of them to their neighbors.

![Balcony Clapping](../_pablo-zuloaga/assets/balcony_clap.jpg)

In countries around the world, doctors are applauded at 8 pm, but this applause has become not only a way of thanking those who help us taking care of the sick or those who are there to help us when we need it. This applauses also support all of us who stay at home, complying with the law and not going out for the benefit of everyone.
                               
![Balcony Michael Jackson](../_pablo-zuloaga/assets/balcony_mj.mp4)

A Balcony party could start with something as simple as playing some good Michael Jackson music, and having some lights hanged from your balcony, and if your neighbors like it, they will answer by turning their cellphone lights, to show you their support, and start moving it around to help the party feeling.

![Balcony Dj](../_pablo-zuloaga/assets/balcony_dj.mp4)

Another way is to take out your whole DJ equipment to the balcony just to cheer up the whole block. You can even add some laser lights (or any kind that you have), smoke effects and a great sound so that everyone can hear them.

![Balcony Party](../_pablo-zuloaga/assets/balcony_party.mp4)

Depending on the king of block you live in, and if the space between the streets is small, and the balconies are big and low, you can go and dance where everyone can see you. This party is in Grece and you can see how everyone sings and dances, cheering up the main "artist".

![Brooklyn Singing](../_pablo-zuloaga/assets/brooklyn_singing.mp4)

But if you even want to do it more epic, you could even put the whole Brooklyn to sing at the rhythm of your music... the important thing is that you play a song you are sure that everyone knows. One of those hits that transcend generations.

![Balcony Table](../_pablo-zuloaga/assets/balcony_table.jpg)

New friendships are created, and the neighbors begin to know each other. Some people generate dynamics among their new friends abroad, like this pair of neighbors who set up a table with a tablecloth from side to side of the balcony where they play cards, drink wine and beer and share time with each other, with the distancing social need. Not very close, but not that far either.

![Sharify1](../_pablo-zuloaga/assets/sharify1.png) ![Sharify2](../_pablo-zuloaga/assets/sharify2.png)

Applications like Sharify help to meet people around you, even being able to promote an event or activity that you want to do among the residents of your neighborhood. Working by geolocation, it helps you interact with people who are close to you and who you probably didn't know. https://sharifyapp.com/

### How to do a balcony event?

* Take advantage of the usual moments of meeting, such as the hours of applause, since several people are leaning on their balconies at that time. The sunny moments of the weekends are other good times since people go out to take advantage of the little sun that reaches the balcony and also that, because it is a weekend, they are less busy.

* If you have telephone contact with the neighbors, a WhatsApp group or an application that helps the connection by proximity (xxxx is a good one), they can help you finalize the details with them.

* Keep in mind that not everyone likes the same music, so you should put something that everyone likes, and it could not be until late at night because it will come.

* Having outdoor lights and good sound is key, but try not to be too much to disturb others. Bulbs with colored lights or Christmas tree lights hanging by the window work very well.

* If your neighbors like music, they will surely take out their cell phones by lighting it with its light on hanging, or they will sing to the rhythm of it as a thank you.

* If you're going to go out on the balcony to demonstrate your skills as an artist and sing a song or play an instrument, make sure you know how to do them well. Your neighbors will thank you, otherwise, although it may go viral, you could receive a big boo.

* Sometimes you can even use costumes or props like glasses, wigs, hats and more to set the show. In the end, the idea is to have fun with everyone and have a good time.

* Another activity you can do, depending on whether you live inside a residential group of buildings or a tight block, is to print cards and distribute them among the neighbors for everyone to play Bingo, in which they meet at a certain time, someone shouts the numbers that come out from their house, while the others write down on the cards that were distributed to them until someone sings "Bingo".

## Idea 2: Live Parties

![Queer Party](../_pablo-zuloaga/assets/live_party_queer.mp4)

One of the most difficult parts for young people is when the weekend arrives when they usually meet up with their friends to go dancing and have a few drinks. So another of the trends that I have found on the web is the "Live Parties" people trying to simulate their real parties in virtual environments.

![Champeta Party](../_pablo-zuloaga/assets/champeta.mp4)

Others gather around a DJ, regardless of whether it is one of them who plays the music or is a video played from YouTube, to dance and have a few drinks. In these cases, everyone dances as if they were in the same place but each one from their own space and even the DJ's work is rotated among the assistants so that everyone can put on music or the DJ sometimes asks what is the next thing they want to hear.

![Live Party](../_pablo-zuloaga/assets/live_party.gif)

This is a live party that I made as an experiment using Jitsi's platform. A link was distributed among friends, who could share it with other friends. These can also be done by zoom and other platforms, but the advantage that Jitsi is that you can invite more than 10 people without limited time and without having a corporate account. 

![Corona Party BCN](../_pablo-zuloaga/assets/Coronapartybcn.png)

You can also share  YouTube videos, which allows you to turn Youtube into the DJ of the party while everyone dances to the same video or a DJ who is broadcasting live from there. The only problem is that Youtube sharing only works on a computer, not on mobile. https://meet.jit.si/

![CovidRoom Site](../_pablo-zuloaga/assets/covidroom_site.png)
![CovidRoom Zoom1](../_pablo-zuloaga/assets/Covidroom_zoom1.png)

Covidroom (https://covidroom.tumblr.com/) is an account that they created in Italy, and they do live parties mixing two platforms, which is another option for everyone who wants to have a party. 

![CovidRoom MixLr](../_pablo-zuloaga/assets/covidroommixlr.png)

They stream the music from a platform called MixLr (https://mixlr.com/covidroom40/) and for the video people can connect to Zoom (https://zoom.us/). 

![CovidRoomIG 1](../_pablo-zuloaga/assets/covidroomIG1.png)

![CovidRoomIG 2](../_pablo-zuloaga/assets/covidroomIG2.png)

This party is promoted through Instagram and Facebook, and streaming can also be seen from streaming platforms for video games such as Twitch and even from streaming platforms of erotic cameras such as Chaturbate.

![CovidRoomIG 2](../_pablo-zuloaga/assets/covidroom_fb.png)

This parties attendedants are people from all over the world, although the majority are mainly from Europe, each one from their homes adding their attitude to the party and interacting with the others.



This parties attendedants are people from all over the world, although the majority are mainly from Europe, each one from their homes adding their attitude to the party and interacting with the others.

## Idea 3: Virtual Traveling

![Pool Party](../_pablo-zuloaga/assets/pool_party.png)

Some others are looking for a way to continue traveling around the world or space without leaving their home, simulating being in summer vacations like this group "Pool Party" shown above, in which 3 people, in different cities of the world (Bogotá (Colombia), Cartagena (Colombia) and Florence (Italy), they meet to chat imagining that they are around a pool, even wearing changing rooms and props according to the theme.


![China Travel](../_pablo-zuloaga/assets/virtual_traveling1.png)

This father who takes his daughters on different routes around the world from the comfort of his living room. He immerses them in an imaginary experience in which they travel to China, London or Space, among other places.

![Space Travel](../_pablo-zuloaga/assets/virtual_traveling2.png)

He pretends to be the pilot of this trip through Boats, Planes or Spaceships, and he teaches them interesting scientific and cultural facts about the places they visit, using google maps, online videos, and others as resources to make the trips more visual. You can find more of their videos on his Instagram account: https://www.instagram.com/diegosantoscaballero/

![Couple Cruise](../_pablo-zuloaga/assets/home_cruise.mp4)

This couple has gone viral right now because they were supposed to be going on a cruise and this was canceled, so they decided to make their cruise from the comfort of their homes. Here you can see them in their robes, enjoying a drink with a video of the sea view in the background and the ambient sound of the cruise. An easy tactic to replicate without leaving home.

![Inflatable Pool 1](../_pablo-zuloaga/assets/pool1.png) ![Inflatable Pool 2](../_pablo-zuloaga/assets/pool2.png) ![Inflatable Pool 3](../_pablo-zuloaga/assets/pool3.png) ![Inflatable Pool 4](../_pablo-zuloaga/assets/pool4.png) ![Inflatable Pool 5](../_pablo-zuloaga/assets/pool5.png)

Some buy an inflatable pool or jacuzzi and enjoy the sun of the spring in the company of family, friends, and some beers. These pools work very well in houses that have terraces or even large balconies. These pools can be ordered through amazon or aliexpress easily.

![Inflatable Pool 6](../_pablo-zuloaga/assets/pool6.jpg)

One problem is that water may lick, and generate humidity in the neighbor's apartment, in that case, filling it with plastic balls can be a good option.

![Movies 1](../_pablo-zuloaga/assets/movies1.png) ![Movies 2](../_pablo-zuloaga/assets/movies2.png) ![Movies 3](../_pablo-zuloaga/assets/movies3.png)

There are also a lot of people, especially those with children, who simulate things like going to the movies inside their home to keep them entertained. They design the tickets, they sell popcorn and snacks, they choose the movie among all and they all enjoy it from their sofa. 

![Store](../_pablo-zuloaga/assets/store.png)

But not only movies or movies are simulated, it has also been seen those who simulate a store for their children inside their home, in which children earn toy money for doing housework, and with these they can buy snacks.

## Idea 4: Family and friends  

![Family Life](../_pablo-zuloaga/assets/family_lunch.png)

Another simulation that I have found very common is the family or friends lunches, in which entire families connect from different places to share breakfast, lunch or dinner. This even creates new moderation labels when speaking on a video conferencing platform, since in general everyone wants to speak at the same time, but they take turns or raise their hand to be able to comment on what the others they comment. Each one talks about how their week was, current notes, health recommendations, and their mood, among many other things.

![Drinks 1](../_pablo-zuloaga/assets/Drinks1.jpg)

One of the social interactions people miss most is having a drink with friends. So to keep in contact with each other, they are meeting at home in front of a webcam, connect to a streaming application around some music to share a good time.

![Drinks 2](../_pablo-zuloaga/assets/Drinks2.png) ![Drinks 3](../_pablo-zuloaga/assets/Drinks3.png)

Recently there is an application that dreams a lot and it is Houseparty (https://app.houseparty.com/login) in which users can get together to play with their friends to guess drawings that others do, make trivia of different themes and other games ... or just to talk and enjoy. This can be downloaded for both Windows and Mac computers or Android and Apple cell phones.


## Questions & Feedback

[pablo.zuloaga.betancourt@students.iaac.net](pablo.zuloaga.betancourt@students.iaac.net)
